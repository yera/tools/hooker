#![warn(rust_2018_idioms)]
#![deny(unused_must_use)]

mod http;
mod routes;
mod router;
mod models;
mod store;

use std::{net::SocketAddr, sync::Arc, collections::HashMap};
use tokio::sync::RwLock;
use router::Router;
use store::HookRegistry;

pub use anyhow::Result;

#[derive(Default, Clone)]
pub struct State {
    pub hooks: HookRegistry,
    pub users: Arc<RwLock<HashMap<String, String>>>,
}

impl State {
    pub fn load() -> Result<Self> {
        log::debug!("Restoring hook configurations");

        Ok(Self {
            hooks: HookRegistry::load()?,
            ..Default::default()
        })
    }
}

#[derive(Debug, argh::FromArgs)]
/// GitLab to Discord webhook server
struct AppArgs {
    #[argh(option, from_str_fn(parse_user))]
    /// an colon separated pair of user and hashed password
    user: Vec<(String, String)>,

    #[argh(switch)]
    /// enables debug logging
    debug: bool,
}

fn parse_user(value: &str) -> Result<(String, String), String> {
    let parts = value.split(':').collect::<Vec<_>>();
    if let [username, password] = parts[..] {
        if password.len() != 64 {
            Err("Password hash has invalid length".into())
        } else {
            Ok((username.to_string(), password.to_string()))
        }
    } else {
        Err("Invalid format, needs to be user:pass".into())
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    use hyper::service::{make_service_fn, service_fn};

    let args: AppArgs = argh::from_env();

    initialize_logger(args.debug)?;

    let state = State::load()?;
    let mut users = state.users.write().await;
    users.extend(args.user);
    drop(users);

    let client = http::Client::new();
    let mut routes = Router::new(client);
    routes.get("/api/hooks", routes::api::get_hooks)
        .post("/api/hook", routes::api::post_hook)
        .delete("/api/hook/:id", routes::api::delete_hook)
        .post("/hooks/gitlab/:id", routes::hooks::post_gitlab);

    let routes = Arc::new(routes);

    let make_service = make_service_fn(move |_conn| {
        let routes = routes.clone();
        let state = state.clone();
        async {
            let service = service_fn(move |req| {
                let routes = routes.clone();
                let state = state.clone();
                async move {
                    let response = routes.handle_route(req, state).await;
                    Ok::<_, hyper::Error>(response)
                }
            });
            Ok::<_, hyper::Error>(service)
        }
    });

    let addr: SocketAddr = ([0, 0, 0, 0], 9292).into();
    let server = hyper::Server::try_bind(&addr)?.serve(make_service);
    log::info!("Starting server on http://{}", addr);
    server.await?;

    Ok(())
}

fn initialize_logger(debug: bool) -> Result<(), Box<dyn std::error::Error>> {
    use simplelog::{TermLogger, Config, TerminalMode};

    let log_level = if debug {
        log::LevelFilter::Debug
    } else {
        log::LevelFilter::Info
    };

    TermLogger::init(log_level, Config::default(), TerminalMode::Mixed)?;

    log::info!("Logger initialized");
    Ok(())
}
